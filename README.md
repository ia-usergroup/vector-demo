# Topic: Vector installation
**NOTE**: with all things, use the 
[official documentation ](https://vector.dev) for best results. All information can be found there.
<!-- TOC -->
- [Topic: Vector Installation](#topic-vector-installation)
  - [Amazon Linux Installation](#amazon-linux-installation)
  - [Docker Non ECS](#docker-non-ecs)
  - [Docker ECS](#docker-ecs)
# Amazon Linux Installation

- directly on amazon linux host

`$ curl -1sLf \
       'https://repositories.timber.io/public/vector/cfg/setup/bash.rpm.sh' \
       | sudo -E bash`

`$ sudo yum install vector`

`$ cd /etc/vector`

`$ vector --version`

`$ cd /etc/vector`

`$ sudo vi vector.toml`

` // change your config file`

`$ sudo systemctl start vector`

`$ sudo systemctl status vector -l`

# Docker Non ECS

`$ docker run \
     -d \
     -v ~/vector.toml:/etc/vector/vector.toml:ro \
     -p 8383:8383 \
     timberio/vector:0.11.X-alpine`
     
 # Docker ECS
 in ecs-docker there is a sample Dockerfile
 I will not be covering all of the steps to run this on ECS.  Maybe at a later date when I have time.
 


